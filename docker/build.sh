#!/bin/bash
#
# (c) https://github.com/MontiCore/monticore
#
# The license generally applicable for this project
# can be found under https://github.com/MontiCore/monticore.
#


cp ../install/rmi-model-server.jar ./
cp -r ../install/softwares ./softwares

docker build -t rmi-server .
