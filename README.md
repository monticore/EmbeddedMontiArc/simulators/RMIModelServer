<!-- (c) https://github.com/MontiCore/monticore -->
[![Build Status](https://travis-ci.org/MontiSim/RMIModelServer.svg?branch=master)](https://travis-ci.org/MontiSim/RMIModelServer)
[![Maintainability](https://api.codeclimate.com/v1/badges/55c8774bfd4d37534691/maintainability)](https://codeclimate.com/github/MontiSim/RMIModelServer/maintainability)

# RMIModelServer

The RMIModelServer is used to remotely allocate a series of [_SoftwareSimulators_](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/hardware_emulator), which can load different softwares generated with [_EMAM2CPP_](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/generators/EMAM2Cpp).

## Requirements

In order the project to be built locally, the following software is required:

    Apache Maven
    Java Development Kit, Version 8+
    (optional) Git

To clone this github repository, one can use the following commands

    cd MontiSim
    git clone https://github.com/MontiSim/RMIModelServer.git
      
__NOTE:__ _MontiSim_ is the directory, in which MontiSim-belonging projects should be clonned. It has to be manually created.

## Compiling 

To just compile the `jar`, use

    mvn clean install -s settings.xml

This will export the resulting jar in the `install` folder

## Running the RMI server

The `install` folder contains an exemplary setup to start the server.
The `run` script starts an instance of the RMIModelServer. It uses the `config` script to provide a way of changing the port or the softwares folder used. The port defaults to 10101, and can be changed to start multiple RMIModelServers.

The softwares folder is a folder path (absolute or relative to the RMIModelServer) in which the RMI server will look for programs to run inside a SoftwareSimulator.
These programs are the EmbeddedMontiArc models compiled with EMAM2CPP (or a project using EMAM2CPP such as [EMAM-showcase](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/simulators/EMAM-showcase) or [EMAStudio](https://git.rwth-aachen.de/monticore/EmbeddedMontiArc/utilities/EMAStudioBuilder)).
The `softwares` folder in the `install` folder contains the sample autopilot from `EMAM-Showcase`.

## Docker

Here is another possibility to run the RMI server without compiling the AutopilotAdapter.dll yourself.

First we build the docker image with 
```
$ ./docker/build.sh
```
This script only does 2 things:

1. Copy the latest rmi-model-server build into ./docker/ in order to prepare the build.
2. Then use ./docker/Dockerfile to build the rmi-model-server docker image.

After the docker image is built. Run following command to start the RMI server with port 10101:

```
$ docker run --name=rmi-server -p 10101:10101 --rm rmi-server
```

Or, for example with port 5000:
```
$ docker run --name=rmi-server -p 5000:10101 --rm rmi-server
```
