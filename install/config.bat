@REM
@REM (c) https://github.com/MontiCore/monticore
@REM
@REM The license generally applicable for this project
@REM can be found under https://github.com/MontiCore/monticore.
@REM


@REM Please replace this with the path to the Autopilot(s)
set SOFTWARES_FOLDER=softwares

@REM Change the port if you need to
set PORT=10101
