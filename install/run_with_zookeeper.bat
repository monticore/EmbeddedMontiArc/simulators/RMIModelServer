@REM
@REM (c) https://github.com/MontiCore/monticore
@REM
@REM The license generally applicable for this project
@REM can be found under https://github.com/MontiCore/monticore.
@REM


@echo off
call config.bat

echo Starting RMIModelServer on port %PORT% with softwares folder: %SOFTWARES_FOLDER%
java -jar rmi-model-server.jar port=%PORT% softwares_folder=%SOFTWARES_FOLDER%
pause
