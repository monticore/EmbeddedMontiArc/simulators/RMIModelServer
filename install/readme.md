<!-- (c) https://github.com/MontiCore/monticore -->
## install

This folder contains a sample setup of the RMIModelServer using the HardwareEmulator.

This folder is used to perform a simple maven test including the HardwareEmulator.

Autopilots can be added in the autopilots folder and loaded on the fly using HardwareEmulator configuration.

The demo folder contains examples that start the RMIModelServer with a default configuration that is added to
every allocated autopilot.
