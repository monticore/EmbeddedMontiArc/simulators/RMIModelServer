#!/bin/bash
#
# (c) https://github.com/MontiCore/monticore
#
# The license generally applicable for this project
# can be found under https://github.com/MontiCore/monticore.
#


# Please replace this with the path to the Autopilot(s)
SOFTWARES_FOLDER=softwares

# Change the port if you need to
PORT=10101
