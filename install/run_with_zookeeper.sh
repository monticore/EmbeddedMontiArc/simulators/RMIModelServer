#!/usr/bin/env bash
#
# (c) https://github.com/MontiCore/monticore
#
# The license generally applicable for this project
# can be found under https://github.com/MontiCore/monticore.
#


source ./config.sh

echo "Starting RMIModelServer on port $PORT with Autopilot folder: $SOFTWARES_FOLDER"
java "-Djava.rmi.server.hostname=localhost" -jar rmi-model-server.jar port=$PORT softwares_folder=$SOFTWARES_FOLDER
