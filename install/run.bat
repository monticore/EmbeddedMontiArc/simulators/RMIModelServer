@REM
@REM (c) https://github.com/MontiCore/monticore
@REM
@REM The license generally applicable for this project
@REM can be found under https://github.com/MontiCore/monticore.
@REM


@echo off
call config.bat

echo Starting RMIModelServer on port %PORT% with with softwares folder: %SOFTWARES_FOLDER%
@REM java "-Djava.rmi.server.codebase=file:rmi-model-server.jar" "-Djava.rmi.server.hostname=localhost" -cp rmi-model-server.jar rwth.rmi.model.server.RMIServer %PORT% %AUTOPILOT_FOLDER% --no-zookeeper %*
java -jar rmi-model-server.jar port=%PORT% softwares_folder=%SOFTWARES_FOLDER% no-zookeeper
pause
