/**
 * (c) https://github.com/MontiCore/monticore
 *
 * The license generally applicable for this project
 * can be found under https://github.com/MontiCore/monticore.
 */
package de.rwth.monticore.EmbeddedMontiArc.simulators.rmimodelserver;

import org.I0Itec.zkclient.ZkClient;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.interfaces.SoftwareSimulatorManager;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.RemoteSoftwareSimulatorManager;
import de.rwth.monticore.EmbeddedMontiArc.simulators.hardware_emulator.config.SoftwareSimulatorConfig;

import java.io.File;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

public class RMIServer {

    public final static String SOFTWARE_SIMULATOR_MANAGER = "SoftwareSimulatorManager";
    public final static String ZK_NODE = "/rmi-servers/all";

    public static void parse_args(String[] args, HashMap<String, String> options){
        for (String arg : args){
            String sub_args[] = arg.split("=");
            if (arg.equals("no-zookeeper")){
                options.put("no-zookeeper", "true");
            } else if (sub_args.length > 1) {
                if (sub_args[0].equals("port")){
                    options.put("port", sub_args[1]);
                } else if (sub_args[0].equals("softwares_folder")){
                    options.put("softwares_folder", sub_args[1]);
                }
            }
            
        }
    }


    public static void main(String[] args) throws UnknownHostException {
        HashMap<String, String> options = new HashMap<String, String>();
        parse_args(args, options);
        if (!options.containsKey("port")){
            System.out.println("Please assign a port that the RMI server need to bind (port=...)");
            return;
        }
        String port = options.get("port");
        if (!isPortAvailable(port)){
            System.out.printf("Port %s is currently unavailable, please use another port.\n", port);
            return;
        }

        File softwares_folder = new File(options.containsKey("softwares_folder") ? options.get("softwares_folder") : "autopilots");
        if (!softwares_folder.exists()){
            System.out.printf("The autopilot folder %s does not exist\n", softwares_folder);
            return;
        }

        boolean use_zookeeper = !options.containsKey("no-zookeeper");
        if (!use_zookeeper)
            System.out.println("Not using Zookeeper.");

        SoftwareSimulatorConfig manager_config = new SoftwareSimulatorConfig();
        try {// Load AutopilotAdapter.dll from libraryPath
            manager_config.set_softwares_folder(softwares_folder.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Could not resolve the SoftwareSimulator's software folder path. ("+softwares_folder+")");
        }

        int port_id = Integer.valueOf(port);
        SoftwareSimulatorManager manager = null;
        try {
            manager = new RemoteSoftwareSimulatorManager(manager_config , port_id);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-2);
        }

        String host = InetAddress.getLocalHost().getHostName();
        try {
            Registry registry = LocateRegistry.createRegistry(port_id);
            System.out.println("RMIRegistry serving on port: " + port);

            //bind the ModelManager to a stub, representing the remote object
            registry.bind(SOFTWARE_SIMULATOR_MANAGER, UnicastRemoteObject.exportObject(manager, port_id));

            if (use_zookeeper){
                String zoo_servers;
                zoo_servers = System.getenv("ZOO_SERVERS");
                if (zoo_servers == null){
                    zoo_servers = "localhost:2181";
                }
                ZkClient zk = new ZkClient(zoo_servers, 1000, 5000);
                register(zk, host, port_id);
                Runtime.getRuntime().addShutdownHook(new Thread(){
                    @Override
                    public void run(){
                        unregister(zk, host, port_id);
                    }
                });
            }

            System.out.println("RMIManager listening");
        } catch (RemoteException | AlreadyBoundException e) {
            e.printStackTrace();
        }

    }

    public static boolean isPortAvailable(String portString) {
        int port;
        try {
            port = Integer.valueOf(portString);
        } catch (NumberFormatException e) {
            System.out.printf("Port %s is illegel, please check your input.", portString);
            throw e;
        }

        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                }
            }
        }
        return false;
    }

    public static void register(ZkClient zk, String host, int port){
        String path = ZK_NODE + "/" + host + ":" + port;
        if (!zk.exists(ZK_NODE)){
            zk.createPersistent(ZK_NODE, true);
        }

        // delete in case a node is already there
        // this usually happens when a simulator was forced shutdown without unregister itself from zookeeper
        zk.delete(path);
        zk.createEphemeral(path);
//        zkPath = path;
        System.out.printf("%s is online\n", path);
    }

    public static void unregister(ZkClient zk,  String host, int port){
        System.out.println("shutting down rmi-server");
        String path = ZK_NODE + "/" + host + ":" + port;
        zk.delete(path);
        System.out.printf("%s is offline\n", path);
    }
}
